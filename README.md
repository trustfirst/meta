# Trust First Protocol

The Trust First Protocol (TFP) is a transmission-agnostic communication protocol that defines a set of handshakes and proceedures that respect and require explicit trust.

## Problem

There are no widely used standards for live communication, human or otherwise, that enable each agent to control their circle of trust. SaaS solutions provide a centralised point of failure. Federated solutions require identity providers. Confederated solutions require establishing a whole node in the confederacy to ensure control. For absolute privacy and trust, a given user must be able to choose to trust a specific user, with no middle party required.

## What does this try to solve?

- Future-proof communications flexible to be held distributed (P2P/direct) or private WAN
- Facilitate secure communications starting from 0 Trust to Full Trust

## What does this not try to solve?

- Find people by an identifier
- Ensure trust cannot be broken
- Protect you from undercover agents

## Requirements

- Fixed steps for connection transformation
  - Engage, disengage
  - Session members change
  - Channels change
- Additive trusts
  - "I trust User X to proxy for User Y"
- Encrypted communication suitable for P2P (QUIC?)
- Scheme-generic at each step post-encryption
  - PGP may not last forever, this protocol should be able to accomodate whatever comes next
- Extensible
  - Extensions for multimedia, etc.
  - To be usable for non-human communication, text messaging cannot be assumed
